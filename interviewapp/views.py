from django.db.models import Count
from django.shortcuts import render
from rest_framework import generics

from .serializers import *
from .models import *


def home(request):
    return render(request, 'interviewapp/public.html')


def search_developer(request):
    return render(request, 'interviewapp/index.html')


def interview(request):
    return render(request, 'interviewapp/interview.html')


class ProgrammingLanguageList(generics.ListAPIView):
    serializer_class = ProgrammingLanguageSerializer

    def get_queryset(self):
        """
        This view should return a list of programming languages
        """
        q = '='
        if self.request.query_params and self.request.query_params['term'] != '':
            q = self.request.query_params['term']

        return ProgrammingLanguage.objects.filter(name__icontains=q)


class LanguageList(generics.ListAPIView):
    serializer_class = LanguageSerializer

    def get_queryset(self):
        """
        This view should return a list of languages
        """
        q = '='
        if self.request.query_params and self.request.query_params['term'] != '':
            q = self.request.query_params['term']

        result = Language.objects.filter(code__icontains=q)
        return result


class DeveloperSearch(generics.ListAPIView):
    serializer_class = DeveloperReadSerializer

    def get_queryset(self):
        """
        This view should return a list developer
        """

        query_pl = self.request.query_params.getlist('progLang[]', None)
        query_l = self.request.query_params.getlist('lang[]', None)

        # query_pl = ['javascript']
        # query_l = ['jp']

        if len(query_l) == 0 and len(query_pl) == 0:
            result = {}
        elif len(query_l) == 0 and len(query_pl) != 0:
            len_p_l = len(query_pl)
            result = Developer.objects \
                .filter(programming_language__name__in=query_pl) \
                .annotate(num_prog_lang=Count('programming_language')) \
                .filter(num_prog_lang__gte=len_p_l)
        elif len(query_pl) == 0 and len(query_l) != 0:
            len_l = len(query_l)
            result = Developer.objects \
                .filter(language__code__in=query_l) \
                .annotate(num_langs=Count('language')) \
                .filter(num_langs__gte=len_l)
        else:
            len_p_l = len(query_pl)
            len_l = len(query_l)
            result = Developer.objects \
                                    .filter(programming_language__name__in=query_pl, language__code__in=query_l) \
                                    .annotate(num_prog_lang=Count('programming_language'), num_langs=Count('language')) \
                                    .filter(num_prog_lang__gte=len_p_l, num_langs__gte=len_l)

        return result


