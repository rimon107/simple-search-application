$(document).ready(function () {
    let _url = $('head base').attr('href');
    $('.progLang-ajax').select2(
        {
            minimumResultsForSearch: Infinity,
            ajax: {
                url: _url + '/programming-languages',
                dataType: 'json',
                type: "GET",
                data: function (params) {
                    var queryParameters = {
                        term: params.term
                    };
                    return queryParameters;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                id: item.name,
                                text: item.name

                            }
                        })
                    };
                }
            }
        }
    );

    $('.lang-ajax').select2(
        {
            minimumResultsForSearch: Infinity,
            ajax: {
                url: _url + '/languages',
                dataType: 'json',
                type: "GET",
                data: function (params) {
                    var queryParameters = {
                        term: params.term
                    };
                    return queryParameters;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                id: item.code,
                                text: item.code

                            }
                        })
                    };
                }
            }
        }
    );
});


var developer = ko.observableArray([
    {
        'id': ko.observable(''),
        'email': ko.observable(''),
        'programming_language': ko.observableArray([
            {
                'name': ko.observableArray([])
            }
        ]),
        'language': ko.observableArray([
            {
                'code': ko.observableArray([])
            }
        ]),
    }
]);

function searchDev() {
    let _url = $('head base').attr('href');
    let progLang = $('.progLang-ajax').val();
    let lang = $('.lang-ajax').val();

    let data = {
        progLang: progLang,
        lang: lang
    };

    $.ajax({
        type: "GET",
        data: data,
        url: _url + '/developer',
        dataType: "json",
        success: function (_data) {

            ko.applyBindings({
                developer: _data
            });
        }
    });

}

function AppViewModel() {
    var self = this;
    let _url = $('head base').attr('href');
    // self.developer = ko.observableArray([
    //     {
    //         'id': ko.observable(''),
    //         'email': ko.observable(''),
    //         'programming_language': ko.observableArray(),
    //         'language': ko.observableArray(),
    //     }
    // ]);
    self.developer = ko.observableArray();

    self.addDeveloper = function () {
        self.developer([]);
        let progLang = $('.progLang-ajax').val();
        let lang = $('.lang-ajax').val();

        let data = {
            progLang: progLang,
            lang: lang
        };

        $.ajax({
            type: "GET",
            data: data,
            url: _url + '/developer',
            dataType: "json",
            success: function (_data) {
                self.developer(_data);
            }
        });

    };

}

ko.applyBindings(new AppViewModel());

