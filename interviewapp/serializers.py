from rest_framework import serializers
from .models import *


class ProgrammingLanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProgrammingLanguage
        fields = ['name']


class LanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Language
        fields = ['code']


class DeveloperSerializer(serializers.ModelSerializer):
    class Meta:
        model = Developer
        fields = '__all__'


class DeveloperReadSerializer(DeveloperSerializer):
    language = LanguageSerializer(many=True, read_only=True)
    programming_language = ProgrammingLanguageSerializer(many=True, read_only=True)


class InterviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Interview
        fields = '__all__'


class InterviewReadSerializer(InterviewSerializer):
    developer = DeveloperSerializer(many=False, read_only=True)
