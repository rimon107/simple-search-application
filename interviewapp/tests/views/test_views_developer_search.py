from django.urls import reverse
from mixer.backend.django import mixer
from rest_framework import status
from rest_framework.test import APITestCase
import pytest
from interviewapp.serializers import ProgrammingLanguageSerializer, LanguageSerializer, DeveloperReadSerializer

pytestmark = pytest.mark.django_db


class DeveloperViewTest(APITestCase):

    def test_developer(self):
        response = self.client.get(reverse('interviewapp:search_developer'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_programming_language_list(self):
        obj = mixer.cycle(100).blend('interviewapp.ProgrammingLanguage', name=mixer.sequence(lambda c: "ja_%s" % c))
        response = self.client.get(reverse('interviewapp:programming_language_list'), {'term': 'ja'}, format='json')
        serializer = ProgrammingLanguageSerializer(obj, many=True)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_language_list(self):
        obj = mixer.cycle(100).blend('interviewapp.Language', code=mixer.sequence(lambda c: "ja_%s" % c))
        response = self.client.get(reverse('interviewapp:language_list'), {'term': 'ja'}, format='json')
        serializer = LanguageSerializer(obj, many=True)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_developer_search_with_programming_language_and_language(self):
        obj_pl = mixer.cycle(100).blend('interviewapp.ProgrammingLanguage', name=mixer.sequence(lambda c: "ja_%s" % c))
        obj_l = mixer.cycle(100).blend('interviewapp.Language', code=mixer.sequence(lambda c: "ja_%s" % c))
        obj = mixer.cycle(100).blend('interviewapp.Developer',
                                     email=mixer.sequence(lambda c: "sa_%s_sn@example.com" % c),
                                     programming_language=obj_pl[1], language=obj_l[1])
        response = self.client.get(reverse('interviewapp:developer_list'), {'progLang[]': ['ja_1'], 'lang[]': ['ja_1']}, format='json')
        serializer = DeveloperReadSerializer(obj, many=True)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_developer_search_with_programming_language(self):
        obj_pl = mixer.cycle(100).blend('interviewapp.ProgrammingLanguage', name=mixer.sequence(lambda c: "ja_%s" % c))
        obj_l = mixer.cycle(100).blend('interviewapp.Language', code=mixer.sequence(lambda c: "ja_%s" % c))
        obj = mixer.cycle(100).blend('interviewapp.Developer',
                                     email=mixer.sequence(lambda c: "sa_%s_sn@example.com" % c),
                                     programming_language=obj_pl[1], language=obj_l[1])
        response = self.client.get(reverse('interviewapp:developer_list'), {'progLang[]': ['ja_1'], 'lang[]': []}, format='json')
        serializer = DeveloperReadSerializer(obj, many=True)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_developer_search_with_language(self):
        obj_pl = mixer.cycle(100).blend('interviewapp.ProgrammingLanguage', name=mixer.sequence(lambda c: "ja_%s" % c))
        obj_l = mixer.cycle(100).blend('interviewapp.Language', code=mixer.sequence(lambda c: "ja_%s" % c))
        obj = mixer.cycle(100).blend('interviewapp.Developer',
                                     email=mixer.sequence(lambda c: "sa_%s_sn@example.com" % c),
                                     programming_language=obj_pl[1], language=obj_l[1])
        response = self.client.get(reverse('interviewapp:developer_list'), {'progLang[]': [], 'lang[]': ['ja_1']}, format='json')
        serializer = DeveloperReadSerializer(obj, many=True)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_developer_search_without_programming_language_and_language(self):
        obj_pl = mixer.cycle(100).blend('interviewapp.ProgrammingLanguage', name=mixer.sequence(lambda c: "ja_%s" % c))
        obj_l = mixer.cycle(100).blend('interviewapp.Language', code=mixer.sequence(lambda c: "ja_%s" % c))
        obj = mixer.cycle(100).blend('interviewapp.Developer',
                                     email=mixer.sequence(lambda c: "sa_%s_sn@example.com" % c),
                                     programming_language=obj_pl[1], language=obj_l[1])
        response = self.client.get(reverse('interviewapp:developer_list'), {'progLang[]': [], 'lang[]': []}, format='json')
        # serializer = DeveloperReadSerializer(obj)

        self.assertEqual(response.data, [])
        self.assertEqual(response.status_code, status.HTTP_200_OK)
