from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
import pytest

pytestmark = pytest.mark.django_db


class HomeViewTest(APITestCase):

    def test_home(self):
        response = self.client.get(reverse('interviewapp:home'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)


