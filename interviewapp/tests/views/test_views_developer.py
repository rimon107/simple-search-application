from django.urls import reverse
from mixer.backend.django import mixer
from rest_framework import status
from rest_framework.test import APITestCase
import pytest
from interviewapp.serializers import DeveloperReadSerializer

pytestmark = pytest.mark.django_db


class DeveloperViewTest(APITestCase):

    def test_get_all_developer(self):
        obj = mixer.cycle(100).blend('interviewapp.Developer')
        response = self.client.get(reverse('developer-list'), format='json')
        serializer = DeveloperReadSerializer(obj, many=True)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_one_developer(self):
        obj = mixer.blend('interviewapp.Developer')
        response = self.client.get(reverse('developer-detail', kwargs={'pk': obj.pk}), format='json')
        serializer = DeveloperReadSerializer(obj)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_developer(self):
        obj_pl = mixer.cycle(100).blend('interviewapp.ProgrammingLanguage', name=mixer.sequence(lambda c: "ja_%s" % c))
        obj_l = mixer.cycle(100).blend('interviewapp.Language', code=mixer.sequence(lambda c: "ja_%s" % c))
        response = self.client.post(reverse('developer-list'), data={'email': 'asdsd@sdddsss.com',
                                                                     'programming_language': [obj_pl[1].id,
                                                                                                  obj_pl[2].id],
                                                                     'language': [obj_l[1].id]})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_developer(self):
        obj_pl = mixer.cycle(100).blend('interviewapp.ProgrammingLanguage', name=mixer.sequence(lambda c: "ja_%s" % c))
        obj_l = mixer.cycle(100).blend('interviewapp.Language', code=mixer.sequence(lambda c: "ja_%s" % c))
        response = self.client.post(reverse('developer-list'), data={'email': 'asdsd',
                                                                     'programming_language': [obj_pl[1].id,
                                                                                                  obj_pl[2].id],
                                                                     'language': [obj_l[1].id]})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_update_developer(self):
        obj_pl = mixer.cycle(100).blend('interviewapp.ProgrammingLanguage', name=mixer.sequence(lambda c: "ja_%s" % c))
        obj_l = mixer.cycle(100).blend('interviewapp.Language', code=mixer.sequence(lambda c: "ja_%s" % c))
        obj = mixer.cycle(100).blend('interviewapp.Developer',
                                     email=mixer.sequence(lambda c: "sa_%s_sn@example.com" % c),
                                     programming_language=obj_pl[1], language=obj_l[1])
        response = self.client.put(reverse('developer-detail', kwargs={'pk': obj[0].pk}), {'email': 'sa_b_sn@example.com',
                                                                     'programming_language': [obj_pl[1].id],
                                                                     'language': [obj_l[1].id]}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_invalid_developer(self):
        obj_pl = mixer.cycle(100).blend('interviewapp.ProgrammingLanguage', name=mixer.sequence(lambda c: "ja_%s" % c))
        obj_l = mixer.cycle(100).blend('interviewapp.Language', code=mixer.sequence(lambda c: "ja_%s" % c))
        obj = mixer.cycle(100).blend('interviewapp.Developer',
                                     email=mixer.sequence(lambda c: "sa_%s_sn@example.com" % c),
                                     programming_language=obj_pl[1], language=obj_l[1])
        response = self.client.put(reverse('developer-detail', kwargs={'pk': obj[0].pk}), {'email': '',
                                                                     'programming_language': [obj_pl[1].id],
                                                                     'language': [obj_l[1].id]}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_partial_update_developer(self):
        obj_pl = mixer.cycle(100).blend('interviewapp.ProgrammingLanguage', name=mixer.sequence(lambda c: "ja_%s" % c))
        obj_l = mixer.cycle(100).blend('interviewapp.Language', code=mixer.sequence(lambda c: "ja_%s" % c))
        obj = mixer.cycle(100).blend('interviewapp.Developer',
                                     email=mixer.sequence(lambda c: "sa_%s_sn@example.com" % c),
                                     programming_language=obj_pl[1], language=obj_l[1])
        response = self.client.patch(reverse('developer-detail', kwargs={'pk': obj[0].pk}), {'email': 'sa_b_sn@example.com',
                                                                     'programming_language': [obj_pl[1].id],
                                                                     'language': [obj_l[1].id]}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_partial_update_invalid_developer(self):
        obj_pl = mixer.cycle(100).blend('interviewapp.ProgrammingLanguage', name=mixer.sequence(lambda c: "ja_%s" % c))
        obj_l = mixer.cycle(100).blend('interviewapp.Language', code=mixer.sequence(lambda c: "ja_%s" % c))
        obj = mixer.cycle(100).blend('interviewapp.Developer',
                                     email=mixer.sequence(lambda c: "sa_%s_sn@example.com" % c),
                                     programming_language=obj_pl[1], language=obj_l[1])
        response = self.client.patch(reverse('developer-detail', kwargs={'pk': obj[0].pk}), {'email': '',
                                                                     'programming_language': [obj_pl[1].id],
                                                                     'language': [obj_l[1].id]}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_delete_developer(self):
        obj_pl = mixer.cycle(100).blend('interviewapp.ProgrammingLanguage', name=mixer.sequence(lambda c: "ja_%s" % c))
        obj_l = mixer.cycle(100).blend('interviewapp.Language', code=mixer.sequence(lambda c: "ja_%s" % c))
        obj = mixer.cycle(100).blend('interviewapp.Developer',
                                     email=mixer.sequence(lambda c: "sa_%s_sn@example.com" % c),
                                     programming_language=obj_pl[1], language=obj_l[1])
        response = self.client.delete(reverse('developer-detail', kwargs={'pk': obj[0].pk}), format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_invalid_developer(self):
        response = self.client.delete(reverse('developer-detail', kwargs={'pk': 33}), format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

