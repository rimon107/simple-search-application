from django.urls import reverse
from mixer.backend.django import mixer
from rest_framework import status
from rest_framework.test import APITestCase
import pytest
from interviewapp.serializers import InterviewReadSerializer

pytestmark = pytest.mark.django_db


class InterviewViewTest(APITestCase):

    def test_interview(self):
        response = self.client.get(reverse('interviewapp:interview'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_all_interview(self):
        obj = mixer.cycle(100).blend('interviewapp.Interview')
        response = self.client.get(reverse('interview-list'), format='json')
        serializer = InterviewReadSerializer(obj, many=True)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_one_interview(self):
        obj = mixer.blend('interviewapp.Interview')
        response = self.client.get(reverse('interview-detail', kwargs={'pk': obj.pk}),  format='json')
        serializer = InterviewReadSerializer(obj)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_interview(self):
        obj = mixer.blend('interviewapp.Developer')
        response = self.client.post(reverse('interview-list'), data={'developer': obj.id,
                                                                     'score': 1,
                                                                     'comment': 'aassss'})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_interview(self):
        response = self.client.post(reverse('interview-list'), data={'developer': '',
                                                                     'score': 1,
                                                                     'comment': 'aassss'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_update_interview(self):
        obj = mixer.blend('interviewapp.Interview')
        response = self.client.put(reverse('interview-detail', kwargs={'pk': obj.pk}), data={'developer': obj.developer.id,
                                                                                            'score': 4,
                                                                                            'comment': 'aassss'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_invalid_interview(self):
        obj = mixer.blend('interviewapp.Interview')
        response = self.client.put(reverse('interview-detail', kwargs={'pk': obj.pk}), data={'developer': obj.developer.id,
                                                                                            'score': 6,
                                                                                            'comment': 'aassss'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_partial_update_interview(self):
        obj = mixer.blend('interviewapp.Interview')
        response = self.client.patch(reverse('interview-detail', kwargs={'pk': obj.pk}), data={'developer': obj.developer.id,
                                                                                            'score': 4,
                                                                                            'comment': 'aassss'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_partial_update_invalid_interview(self):
        obj = mixer.blend('interviewapp.Interview')
        response = self.client.patch(reverse('interview-detail', kwargs={'pk': obj.pk}), data={'developer': obj.developer.id,
                                                                                            'score': 6,
                                                                                            'comment': 'aassss'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_delete_interview(self):
        obj = mixer.blend('interviewapp.Interview')
        response = self.client.delete(reverse('interview-detail', kwargs={'pk': obj.pk}), format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_invalid_interview(self):
        response = self.client.delete(reverse('interview-detail', kwargs={'pk': 30}), format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

