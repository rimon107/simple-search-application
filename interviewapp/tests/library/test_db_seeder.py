import pytest
from django.test import TestCase

from database_seeder import *
pytestmark = pytest.mark.django_db


class InterviewValidationTest(TestCase):

    def setUp(self):
        self.n = 5

    def test_generate_fake_programming_language(self):
        result = generate_fake_programming_language(self.n)
        length = len(result)
        assert length == self.n

    def test_generate_fake_language(self):
        result = generate_fake_language(self.n)
        length = len(result)
        assert length == self.n

    def test_generate_fake_developer(self):
        result = generate_fake_developer(self.n)
        length = len(result)
        assert length == self.n

    def test_generate_fake_interview(self):
        result = generate_fake_interview(self.n)
        length = len(result)
        assert length == self.n

