import pytest
from mixer.backend.django import mixer
pytestmark = pytest.mark.django_db


class TestModel:
    def test_language_model(self):
        obj = mixer.blend('interviewapp.Language')
        assert obj.pk == 1, 'Should save an instance'

    def test_programming_language_model(self):
        obj = mixer.blend('interviewapp.ProgrammingLanguage')
        assert obj.pk == 1, 'Should save an instance'

    def test_developer_model(self):
        obj = mixer.blend('interviewapp.Developer')
        assert obj.pk == 1, 'Should save an instance'

    def test_interview_model(self):
        obj = mixer.blend('interviewapp.Interview')
        assert obj.pk == 1, 'Should save an instance'




