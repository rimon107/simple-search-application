from django.test import TestCase
from interviewapp.models import *
import pytest
from django.core.exceptions import ValidationError
from mixer.backend.django import mixer
pytestmark = pytest.mark.django_db


class InterviewValidationTest(TestCase):

    def setUp(self):
        pass

    def test_default_interview_model_less_than_1(self):
        obj = mixer.blend('interviewapp.Developer')
        i = Interview(
            developer=obj,
            score=0,
            comment="asd"
        )
        with self.assertRaises(ValidationError):
            i.full_clean()

    def test_default_interview_model_greater_than_5(self):
        obj = mixer.blend('interviewapp.Developer')
        i = Interview(
            developer=obj,
            score=6,
            comment="asd"
        )
        try:
            i.full_clean()
        except ValidationError as e:
            self.assertTrue(i.score,'Ensure this value is less than or equal to 5.')


    def tearDown(self):
        pass