from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.contrib.auth.models import User

from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

# def validate_even(value):
#     if value >= 1 and value <=5 :
#         pass
#     else:
#         raise ValidationError(
#             _('%(value)s is not an even number'),
#             params={'value': value},)

# Create your models here.
class ProgrammingLanguage(models.Model):
    name = models.CharField(max_length=255, blank=False, null=False)

    def __str__(self):
        return self.name


class Language(models.Model):
    code = models.CharField(max_length=255, blank=False, null=False)

    def __str__(self):
        return self.code


class Developer(models.Model):
    email = models.EmailField(blank=False, null=False)
    programming_language = models.ManyToManyField(ProgrammingLanguage)
    language = models.ManyToManyField(Language)

    def __str__(self):
        return self.email


class Interview(models.Model):
    developer = models.ForeignKey(Developer, on_delete=models.CASCADE)
    score = models.PositiveIntegerField(default=3, validators=[MinValueValidator(1), MaxValueValidator(5)], blank=False, null=False)
    # score = models.PositiveIntegerField(default=3, validators=[validate_even], blank=False, null=False)
    comment = models.TextField(max_length=1000, blank=True, null=True)

    def __str__(self):
        return self.developer.email


