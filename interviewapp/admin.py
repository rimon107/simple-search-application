from django.contrib import admin
from interviewapp.models import *
# Register your models here.

admin.site.register(Developer)
admin.site.register(ProgrammingLanguage)
admin.site.register(Language)
admin.site.register(Interview)