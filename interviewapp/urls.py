from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from .views import *

app_name = 'interviewapp'

urlpatterns = [
    path('', home, name='home'),
    path('programming-languages/', ProgrammingLanguageList.as_view(), name='programming_language_list'),
    path('languages/', LanguageList.as_view(), name='language_list'),
    path('developer/', DeveloperSearch.as_view(), name='developer_list'),
    path('interview/', interview, name='interview'),
    path('search-developer/', search_developer, name='search_developer')
]

urlpatterns = format_suffix_patterns(urlpatterns)