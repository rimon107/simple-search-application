from interviewapp.models import *


def not_used_programming_languages():
    result = ProgrammingLanguage.objects.filter(developer__email__isnull=True).distinct()
    return result
