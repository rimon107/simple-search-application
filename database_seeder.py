import os

from django.db import IntegrityError

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'interview.settings')
import django

django.setup()

from interviewapp.models import *
from random import seed, sample, randint, choice
from faker import Faker

fake = Faker()
seed()


def generate_fake_programming_language(count=100):
    pl_list = []
    for i in range(count):
        pl = ProgrammingLanguage(
            name=fake.first_name(),
        )

        pl_list.append(pl)

    ProgrammingLanguage.objects.bulk_create(pl_list)
    result = ProgrammingLanguage.objects.values_list('id', flat=True)
    return result


def generate_fake_language(count=100):
    l_list = []
    for i in range(count):
        lang = Language(
            code=fake.language_code(),
        )

        l_list.append(lang)

    Language.objects.bulk_create(l_list)
    result = Language.objects.values_list('id', flat=True)
    return result


def generate_fake_developer(count=100):
    prog_lang = ProgrammingLanguage.objects.values_list('id', flat=True)

    if prog_lang.count() == 0:
        prog_lang = generate_fake_programming_language(count)
        # prog_lang = prog_lang_obj.values_list('id', flat=True)

    prog_lang_count = len(prog_lang)
    prog_lang_index = randint(1, prog_lang_count)

    lang = Language.objects.values_list('id', flat=True)
    if lang.count() == 0:
        lang = generate_fake_language(count)
        # lang = lang_obj.values_list('id', flat=True)
    lang_count = len(lang)
    lang_index = randint(1, lang_count)

    dev_list = []
    for i in range(count):
        dev = Developer(
            email=fake.email()
        )
        dev.save()
        programming_language_id = sample(list(prog_lang), prog_lang_index)
        pl = ProgrammingLanguage.objects.filter(id__in=programming_language_id)
        dev.programming_language.set(pl)

        language_id = sample(list(lang), lang_index)
        l = Language.objects.filter(id__in=language_id)
        dev.language.set(l)

        dev_list.append(dev)

    result = Developer.objects.values_list('id', flat=True)
    return result


def generate_fake_interview(count=100):
    dev = Developer.objects.values_list('id', flat=True)
    intv_list = []

    if dev.count() == 0:
        dev = generate_fake_developer(count)

    for i in range(count):
        intrv = Interview(
            developer_id=choice(dev),
            score=randint(1,5),
            comment=fake.text()
        )

        intv_list.append(intrv)

    result = Interview.objects.bulk_create(intv_list)
    return result



# generate_fake_programming_language(100)
# generate_fake_language(100)
# generate_fake_developer(100)
# generate_fake_interview(100)

