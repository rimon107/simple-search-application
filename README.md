=========================
SIMPLE SEARCH APPLICATION
=========================

The project is written in python (Django Framework). It is managed by git and hosted in bitbucket in the following URL:

https://bitbucket.org/rimon107/simple-search-application/src/master/ 

Live version of the project is available on the following URL:

https://interview-search.herokuapp.com/

To run the project on local development environment, follow the steps bellow:

* Create a virtual environment.

* Run command in the virtual environment

         pip install �r requirements.txt

* To run the test codes, run the following command in the project root directory 

         py.test
        
* Create database for the project using the following commands 

        python manage.py makemigrations
        python manage.py migrate
                
* generate fake data
                    
        1. uncomment last 4 lines in database_seeder.py
        2. in the project root directory run command: python database_seeder.py
        
* run the project using the command

        python manage.py runserver

The project will be running in `http://127.0.0.1:8000/` 

The test code coverage reports are in **htmlcov** folder. 
