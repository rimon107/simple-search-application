$(document).ready(function () {
    $('.progLang-ajax').select2(
        {
            minimumResultsForSearch: Infinity,
            ajax: {
                url: 'http://127.0.0.1:8000/programming-languages',
                dataType: 'json',
                type: "GET",
                data: function (params) {
                    var queryParameters = {
                        term: params.term
                    };
                    return queryParameters;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                id: item.name,
                                text: item.name

                            }
                        })
                    };
                }
            }
        }
    );

    $('.lang-ajax').select2(
        {
            minimumResultsForSearch: Infinity,
            ajax: {
                url: 'http://127.0.0.1:8000/languages',
                dataType: 'json',
                type: "GET",
                data: function (params) {
                    var queryParameters = {
                        term: params.term
                    };
                    return queryParameters;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                id: item.code,
                                text: item.code

                            }
                        })
                    };
                }
            }
        }
    );
});


var developer = ko.observableArray([
    {
        'id': ko.observable(''),
        'email': ko.observable(''),
        'programming_language': ko.observableArray(),
        'language': ko.observableArray(),
    }
]);

function searchDev() {

    let progLang = $('.progLang-ajax').val();
    let lang = $('.lang-ajax').val();

    let data = {
        progLang: progLang,
        lang: lang
    };

    $.ajax({
        type: "GET",
        data: data,
        url: 'http://127.0.0.1:8000/developer',
        dataType: "json",
        success: function (data) {

            ko.applyBindings({
                developer: data
            });
        }
    });

}

function AppViewModel() {
    var self = this;

    // self.developer = ko.observableArray([
    //     {
    //         'id': ko.observable(''),
    //         'email': ko.observable(''),
    //         'programming_language': ko.observableArray(),
    //         'language': ko.observableArray(),
    //     }
    // ]);
    self.developer = ko.observableArray();

    self.addDeveloper = function () {
        self.developer([]);
        let progLang = $('.progLang-ajax').val();
        let lang = $('.lang-ajax').val();

        let data = {
            progLang: progLang,
            lang: lang
        };

        $.ajax({
            type: "GET",
            data: data,
            url: 'http://127.0.0.1:8000/developer',
            dataType: "json",
            success: function (data) {
                self.developer(data);
            }
        });

    };

}

ko.applyBindings(new AppViewModel());

