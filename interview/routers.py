from rest_framework import routers
from interviewapp.viewsets import InterviewViewSet, DeveloperViewSet

router = routers.DefaultRouter()

router.register(r'interview', InterviewViewSet)
router.register(r'developer', DeveloperViewSet)
